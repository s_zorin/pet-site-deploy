#!/usr/bin/env bash
#
# Required globals:
#   USER
#   SERVER
#   REMOTE_ROOT
#   LOCAL_ROOT
#
# Optional globals:
#   SITE_DIRS   (default: "html")
#   DB_DIR      (default: "database")
#   SSH_KEY
#   EXTRA_ARGS
#   DEBUG
#   SSH_PORT
#   SSH_ARGS
#   DELETE_FLAG

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

validate() {
  # mandatory parameters
  : USER=${USER:?'SSH_USER variable missing.'}
  : SERVER=${SERVER:?'SSH_SERVER variable missing.'}
  : REMOTE_ROOT=${REMOTE_ROOT:?'REMOTE_ROOT variable missing.'}
  : LOCAL_ROOT=${LOCAL_ROOT:?'LOCAL_ROOT variable missing.'}
  : SITE_DIRS=${SITE_DIRS:="html"}
  : DB_DIR=${DB_DIR:="database"}
  : DEBUG=${DEBUG:="false"}
  : DELETE_FLAG=${DELETE_FLAG:="true"}

  ARGS_STRING=""


  if [ -n "${DELETE_FLAG}" ]; then
    if [ "${DELETE_FLAG}" == "true" ]; then
      ARGS_STRING="${ARGS_STRING} --delete-after"
    elif [ "${DELETE_FLAG}" != "false" ]; then
      fail "Value of DELETE_FLAG: ${DELETE_FLAG}, has to be true | false."
    fi
  fi

  if [ ! -z ${EXTRA_ARGS_COUNT} ]; then
    init_array_var 'EXTRA_ARGS'; 
  else
    IFS=' ' read -r -a EXTRA_ARGS <<< "${EXTRA_ARGS}"
  fi

  SSH_ARGS=${SSH_ARGS:="-o StrictHostKeyChecking=no"}
  if [[ ! -z "${SSH_PORT}" ]]; then
    SSH_ARGS="-p $SSH_PORT $SSH_ARGS"
  fi
}



setup_ssh_dir() {
  INJECTED_SSH_CONFIG_DIR="/opt/atlassian/pipelines/agent/ssh"
  # The default ssh key with open perms readable by alt uids
  IDENTITY_FILE="${INJECTED_SSH_CONFIG_DIR}/id_rsa_tmp"
  # The default known_hosts file
  KNOWN_HOSTS_FILE="${INJECTED_SSH_CONFIG_DIR}/known_hosts"

  mkdir -p ~/.ssh || debug "adding ssh keys to existing ~/.ssh"
  touch ~/.ssh/authorized_keys

  # If given, use SSH_KEY, otherwise check if the default is configured and use it
  if [ "${SSH_KEY}" != "" ]; then
     debug "Using passed SSH_KEY"
     (umask  077 ; echo ${SSH_KEY} | base64 -d > ~/.ssh/pipelines_id)
  elif [ ! -f ${IDENTITY_FILE} ]; then
     error "No default SSH key configured in Pipelines."
     exit 1
  else
     debug "Using default ssh key"
     cp ${IDENTITY_FILE} ~/.ssh/pipelines_id
  fi

  if [ ! -f ${KNOWN_HOSTS_FILE} ]; then
      error "No SSH known_hosts configured in Pipelines."
      exit 2
  fi

  cat ${KNOWN_HOSTS_FILE} >> ~/.ssh/known_hosts
  if [ -f ~/.ssh/config ]; then
      debug "Appending to existing ~/.ssh/config file"
  fi
  echo "IdentityFile ~/.ssh/pipelines_id" >> ~/.ssh/config
  chmod -R go-rwx ~/.ssh/
}

copy_dir() {
  local dir=$( echo "$1/" | sed 's#//$#/#' )

  if [[ ! -z "${SSH_ARGS}" ]]; then
    run rsync -rp ${ARGS_STRING} ${RSYNC_DEBUG_ARGS} "${EXTRA_ARGS[@]}" -e "ssh ${SSH_ARGS}" ${LOCAL_ROOT}/${dir} ${USER}@${SERVER}:${REMOTE_ROOT}/${dir}
  else
    run rsync -rp ${ARGS_STRING} ${RSYNC_DEBUG_ARGS} "${EXTRA_ARGS[@]}" ${LOCAL_ROOT}/${dir} ${USER}@${SERVER}:${REMOTE_ROOT}/${$dir}
  fi

  if [[ "${status}" == "0" ]]; then
    success "'$dir' deployed."
  else
    fail "'$dir' deployment failed."
  fi

} 
  

run_pipe() {
  info "Starting deployment to ${SERVER}:${REMOTE_ROOT}..."

  info  "SITE_DIRS: '${SITE_DIRS}'"
  info  "DB_DIR: '${DB_DIR}'"
  info  ${SITE_DIRS} ${DB_DIR}

  for dir in ${SITE_DIRS} ${DB_DIR}; do
    info " copy_dir '$dir'"
    copy_dir $dir
  done
}


validate
enable_debug
setup_ssh_dir
run_pipe

success "Deployment finished."
exit $status
