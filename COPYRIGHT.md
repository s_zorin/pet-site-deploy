Copyright © 2022, Sergey Zorin <sergey@zorin.ru>. All rights reserved.

pet-site-deploy is free software: 
you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

pet-site-deploy is distributed 
in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Gitlab to Bitbucket Alias in [LICENSE.txt](LICENSE.txt).
If not, see <http://www.gnu.org/licenses/>.
