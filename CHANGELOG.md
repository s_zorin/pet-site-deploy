# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.5

- patch: remove exit

## 0.0.4

- patch: add debug

## 0.0.3

- patch: fix bug

## 0.0.2

- patch: readme

## 0.0.1

- patch: Initial version
